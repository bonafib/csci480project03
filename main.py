# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 13:15:58 2014

@author: bonafib
"""
import os
from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram
import pygame
from pygame.locals import *
import numpy as N
from ctypes import c_void_p

from cylinder import *
from transforms import *
from controls import *
from frame import *
from floor import *
from texture import loadTexture
from cone import *
import random


def loadFile(filename):
    with open(os.path.join(os.getcwd(), filename)) as fp:
        return fp.read()
        
def compileShaderProgram(vertexShader, fragmentShader):
    """
    Instead of calling OpenGL's shader compilation functions directly
    (glShaderSource, glCompileShader, etc), we use PyOpenGL's wrapper
    functions, which are much simpler to use.
    """
    myProgram = compileProgram(
        compileShader(vertexShader, GL_VERTEX_SHADER),
        compileShader(fragmentShader, GL_FRAGMENT_SHADER)
    )
    return myProgram

def main ():
    # pygame initialization
    global cyl
    pygame.init()
    screen = pygame.display.set_mode((1024,768), OPENGL|DOUBLEBUF)
    clock = pygame.time.Clock()
    
    frame = Frame()    
    
    # OpenGL initializations
    glClearColor(0.5,0.0,0.4,0.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LESS)
 
    # make vertex array
    VertexArrayID = glGenVertexArrays(1)
    glBindVertexArray(VertexArrayID)
   

    # compile shaders
    vertexShader = loadFile(os.path.join("shaders","TransformVertexShader.vertexshader"))
    fragmentShader = loadFile(os.path.join("shaders","TextureFragmentShader.fragmentshader"))
    programID = compileShaderProgram(vertexShader, fragmentShader)

    #make eye and fwd vec
    eye = vec([6,6,6])
    focus = vec([0,0,0])

    # get handle for "MVP" uniform
    MatrixID = glGetUniformLocation(programID, "MVP")
    
    control = Control()
                                          
    # projection matrix
    Projection = control.getProjectionMatrix()
    # view matrix
    View = control.getViewMatrix()
    # model matrix
    Model = control.getModelMatrix()
    
    MVP = N.dot(Projection, N.dot(View, Model))
    MVP = N.array(MVP, dtype=N.float32)
    
        # fill data array
    g_vertex_buffer_data = N.array((-1,-1,0,
                                    1,-1,0,
                                    0,1,0,),dtype=N.float32)
    b,l,f = -5,-1,-1
    t,r,n = -3,1,1
    bln = [b,l,n,1]
    blf = [b,l,f,1]
    brn = [b,r,n,1]
    brf = [b,r,f,1]
    tln = [t,l,n,1]
    tlf = [t,l,f,1]
    trn = [t,r,n,1]
    trf = [t,r,f,1]
    bot = bln+blf+brn + brn+blf+brf
    top = tln+tlf+trn + trn+tlf+trf
    left = bln+blf+tlf + bln+tlf+tln
    right = brn+trf+brf + brn+trn+trf
    front = bln+tln+trn + bln+trn+brn
    back = blf+trf+tlf + blf+brf+trf
    
    ll = [1,0]
    lr = [1,1]
    ul = [0,0]
    ur = [0,1]
    cot = ul+ll+ur + ur+ll+lr
    cop = ul+ll+ur + ur+ll+lr
    ceft = ul+ll+lr + ul+ll+ur
    cight = lr+ul+ll + lr+ur+ul
    cront = ll+ul+ur + ll+ur+lr
    cack = ul+ll+ur + ur+ll+lr
    
    uvCube = cot+cop+cront+cack+ceft+cight
    
    b2,l2,f2 = 3,-1,-1
    t2,r2,n2 = 5,1,1
    bln2 = [b2,l2,n2,1]
    blf2 = [b2,l2,f2,1]
    brn2 = [b2,r2,n2,1]
    brf2 = [b2,r2,f2,1]
    tln2 = [t2,l2,n2,1]
    tlf2 = [t2,l2,f2,1]
    trn2 = [t2,r2,n2,1]
    trf2 = [t2,r2,f2,1]
    bot2 = bln2+blf2+brn2 + brn2+blf2+brf2
    top2 = tln2+tlf2+trn2 + trn2+tlf2+trf2
    left2 = bln2+blf2+tlf2 + bln2+tlf2+tln2
    right2 = brn2+trf2+brf2 + brn2+trn2+trf2
    front2 = bln2+tln2+trn2 + bln2+trn2+brn2
    back2 = blf2+trf2+tlf2 + blf2+brf2+trf2
    
    
    
    b,l,f = -1,-1,-5
    t,r,n = 1,1,-3
    bln2 = [b,l,n,1]
    blf2 = [b,l,f,1]
    brn2 = [b,r,n,1]
    brf2 = [b,r,f,1]
    tln2 = [t,l,n,1]
    tlf2 = [t,l,f,1]
    trn2 = [t,r,n,1]
    trf2 = [t,r,f,1]
    bot3 = bln2+blf2+brn2 + brn2+blf2+brf2
    top3 = tln2+tlf2+trn2 + trn2+tlf2+trf2
    left3 = bln2+blf2+tlf2 + bln2+tlf2+tln2
    right3 = brn2+trf2+brf2 + brn2+trn2+trf2
    front3 = bln2+tln2+trn2 + bln2+trn2+brn2
    back3 = blf2+trf2+tlf2 + blf2+brf2+trf2
    
    b,l,f = -1,-1,3
    t,r,n = 1,1,5
    bln2 = [b,l,n,1]
    blf2 = [b,l,f,1]
    brn2 = [b,r,n,1]
    brf2 = [b,r,f,1]
    tln2 = [t,l,n,1]
    tlf2 = [t,l,f,1]
    trn2 = [t,r,n,1]
    trf2 = [t,r,f,1]
    bot4 = bln2+blf2+brn2 + brn2+blf2+brf2
    top4 = tln2+tlf2+trn2 + trn2+tlf2+trf2
    left4 = bln2+blf2+tlf2 + bln2+tlf2+tln2
    right4 = brn2+trf2+brf2 + brn2+trn2+trf2
    front4 = bln2+tln2+trn2 + bln2+trn2+brn2
    back4 = blf2+trf2+tlf2 + blf2+brf2+trf2

 
    cube2 = bot2+top2+front2+back2+left2+right2
    cube3 = bot3+top3+front3+back3+left3+right3
    cube4 = bot4+top4+front4+back4+left4+right4
    g_vertex_buffer_data = vec(bot+top+front+back+left+right + cube2+cube3+cube4) 
    g_uv_buffer_data = vec(uvCube+uvCube+uvCube+uvCube)
    
    # vertex buffer
    vertexbuffer = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer)
    glBufferData(GL_ARRAY_BUFFER, g_vertex_buffer_data, GL_STATIC_DRAW)
    # color buffer
    uvbuffer = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
    glBufferData(GL_ARRAY_BUFFER, g_uv_buffer_data, GL_STATIC_DRAW)
    # get IDs from program
    positionID = glGetAttribLocation(programID, "vertexPosition_modelspace")
    uvID = glGetAttribLocation(programID, "vertexUV")
    print uvID, positionID
    print "hello"
    
    # texture
    Texture = loadTexture(os.path.join("images","brickwork-bump-map.jpg"))
    # get a handle
    TextureID = glGetUniformLocation(programID, "myTextureSampler")
     
    Texture2 = loadTexture(os.path.join("images","brickwork-texture.jpg"))
    # get a handle
    TextureID2 = glGetUniformLocation(programID, "myTextureSampler")     
    
    
    Texture3 = loadTexture(os.path.join("images","fHills.png"))
    # get a handle
    TextureID3 = glGetUniformLocation(programID, "myTextureSampler") 
    #trying to add texture

    running = True
    while running:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            if event.type == KEYUP and event.key == K_ESCAPE:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                control.handleMouseButton(event.button)
        control.handleMouseMotion()
        control.handleKeyboard()
        
        
        # compute MVP
        Projection = control.getProjectionMatrix()
        View = control.getViewMatrix()
        Model = scaleMatrix(1,1,1)
        MVP = N.dot(Projection, N.dot(View, Model))
        MVP = N.array(MVP, dtype=N.float32)
        # draw into opengl context
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        # use our shader
        glUseProgram(programID)

        # bind our texture to texture unit
        textureUnit = 4
        glActiveTexture(GL_TEXTURE0 + textureUnit)
        # use our texture data
        glBindTexture(GL_TEXTURE_2D, Texture)
        # set our sampler to use texture unit
        glUniform1i(TextureID, textureUnit)      
        
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
        glBindVertexArray(VertexArrayID)        
        
        # disable after drawing command
        glEnableVertexAttribArray(positionID)
       
        # 1st attribute buffer:  vertices
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer)
        glVertexAttribPointer(
            0,             # attrib 0, no reason
            4,             # size
            GL_FLOAT,      # type
            GL_FALSE,      # normalized?
            0,             # stride
            c_void_p(0)    # array buffer offset
            )
            
        
        # 2nd attribute buffer: uvs
        glEnableVertexAttribArray(uvID)
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
        glVertexAttribPointer(
            uvID,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            c_void_p(0))
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, 48*4) # 3 indices starting at 0
        glDisableVertexAttribArray(0)
        glDisableVertexAttribArray(1)
        
        for i in range (1,5):
            
            #draw perimeter
            gl_vertex_buffer_data = vec(bot+top+front+back+left+right)
            gl_uv_buffer_data = vec(uvCube+uvCube+uvCube+uvCube)

#            gl_color_buffer_data = vec([N.random.random() for x in range(len(gl_vertex_buffer_data))])
#            gl_color_buffer_data = N.copy(g_vertex_buffer_data)*0.5 + 0.5
            
            gvertexbuffer = glGenBuffers(1)        
            glBindBuffer(GL_ARRAY_BUFFER, gvertexbuffer)
            glBufferData(GL_ARRAY_BUFFER, gl_vertex_buffer_data,GL_STATIC_DRAW)
            
            gcolorbuffer = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, gcolorbuffer)
            glBufferData(GL_ARRAY_BUFFER, gl_uv_buffer_data, GL_STATIC_DRAW)        
            
            Projection = control.getProjectionMatrix()
            View = control.getViewMatrix()
            if i == 1:
                Model = N.dot(N.dot(rotationYMatrix(0.8),scaleMatrix(3,0.75,0.75)),translationMatrix(4,0,3.5))
            if i == 2:
                Model = N.dot(N.dot(rotationYMatrix(2.4),scaleMatrix(3,0.75,0.75)),translationMatrix(4,0,3.5))
            if i == 3:
                Model = N.dot(N.dot(rotationYMatrix(3.8),scaleMatrix(3,0.75,0.75)),translationMatrix(4,0,3.5))
            if i == 4:
                Model = N.dot(N.dot(rotationYMatrix(5.6),scaleMatrix(3,0.75,0.75)),translationMatrix(4,0,3.5))


                
            MVP = N.dot(Projection, N.dot(View, Model))
            MVP = N.array(MVP, dtype=N.float32)
            #draw into opengl context
            
            
                
            # use our shader
            glUseProgram(programID)
            
            glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
            
            glBindVertexArray(VertexArrayID)        
            # disable after drawing command
            glEnableVertexAttribArray(positionID)
           
            # 1st attribute buffer:  vertices
            glEnableVertexAttribArray(0)
            glBindBuffer(GL_ARRAY_BUFFER, gvertexbuffer)
            glVertexAttribPointer(
                0,             # attrib 0, no reason
                4,             # size
                GL_FLOAT,      # type
                GL_FALSE,      # normalized?
                0,             # stride
                c_void_p(0)    # array buffer offset
                )
                
            # 2nd attribute buffer: uvs
            glEnableVertexAttribArray(uvID)
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
            glVertexAttribPointer(
                uvID,
                2,
                GL_FLOAT,
                GL_FALSE,
                0,
                c_void_p(0))
            # draw the triangle
            glDrawArrays(GL_TRIANGLES, 0, 12*3) # 3 indices starting at 0

        #put a tower in center
        cyl = Cylinder(fragShader = 'knotfragment6.fs')        
        cyl.draw({},{},{'vMatrix': control.getViewMatrix(),
                                  'pMatrix':control.getProjectionMatrix(),
                                  'mMatrix':scaleMatrix(1,1,1)})
        
        cyl2 = Cylinder(height=2.0, radius=1.0, fragShader = 'knotfragment2.fs')
        cyl2.draw({'time':0.0,
                   'useKnot':1.0,
                   'fogEnd':0.0},{},{'vMatrix': control.getViewMatrix(),
                                  'pMatrix':control.getProjectionMatrix(),
                                  'mMatrix':translationMatrix(7,0,5)})
        cyl3 = Cylinder(height=2.0, radius=1.0, fragShader = 'knotfragment2.fs')
        cyl3.draw({'time':0.0,
                   'useKnot':1.0,
                   'fogEnd':0.0},{},{'vMatrix': control.getViewMatrix(),
                                  'pMatrix':control.getProjectionMatrix(),
                                  'mMatrix':translationMatrix(5,0,7)})
        
        for i in range(1,5):
            tempCyl = Cylinder(height=4.0, radius=3.8, fragShader = 'knotfragment3.fs')
            if(i == 1):            
                tempCyl.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-10,0,-12)})
                cone = Cone(height=2.0, radius=2.0)
                cone.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-10,5,-12)})                
                
            if(i == 2):
                tempCyl.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-14,0,-13)})
                cone = Cone(height=2.0, radius=2.0)
                cone.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-14,5,-13)})
            if(i == 3):
                tempCyl.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-8,0,-15)})
                cone = Cone(height=2.0, radius=2.0)
                cone.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-8,5,-15)})
            if(i == 4):
                tempCyl.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-13,0,-10)})
                cone = Cone(height=2.0, radius=2.0)
                cone.draw({'time':0.0,
                              'useKnot':0.0,
                              'fogEnd':0.0},{},
                               {'vMatrix': control.getViewMatrix(),
                               'pMatrix':control.getProjectionMatrix(),
                               'mMatrix':translationMatrix(-13,5,-10)})
            
             
        flo = Floor()
        flo.draw({},{},{'vMatrix': control.getViewMatrix(),
                                  'pMatrix':control.getProjectionMatrix(),
                                  'mMatrix':scaleMatrix(1,1,1)})
        for i in range(1,12):                      
            urSquare = [1,-.48,1,1]
            ulSquare = [-1,-.48,1,1]
            lrSquare = [1,-.48,-1,1]
            llSquare = [-1,-.48,-1,1]
            square = llSquare+ulSquare+urSquare + llSquare+lrSquare+urSquare
            
            gr_vertex_buffer_data = vec(square)
            gr_uv_buffer_data = vec([0,0]+[0,1]+[1,1]+[0,0]+[1,0]+[1,1])
            
            grvertexbuffer = glGenBuffers(1)        
            glBindBuffer(GL_ARRAY_BUFFER, grvertexbuffer)
            glBufferData(GL_ARRAY_BUFFER, gr_vertex_buffer_data,GL_STATIC_DRAW)
            
            grcolorbuffer = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, grcolorbuffer)
            glBufferData(GL_ARRAY_BUFFER, gr_uv_buffer_data, GL_STATIC_DRAW)
            
            Projection = control.getProjectionMatrix()
            View = control.getViewMatrix()
            
            if i == 1:
                Model = N.dot(translationMatrix(3.1,0,3.1),rotationYMatrix(0.8))
            if i >1:
                Model = N.dot(Model,translationMatrix(2,0,0))

            MVP = N.dot(Projection, N.dot(View, Model))
            MVP = N.array(MVP, dtype=N.float32)
            #draw into opengl context
            
            
                
            # use our shader
            glUseProgram(programID)
            
                    # bind our texture to texture unit
            textureUnit = 4
            glActiveTexture(GL_TEXTURE0 + textureUnit)
            # use our texture data
            glBindTexture(GL_TEXTURE_2D, Texture2)
            # set our sampler to use texture unit
            glUniform1i(TextureID2, textureUnit)      
        
            glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
            glBindVertexArray(VertexArrayID)    
            
            glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
            
            glBindVertexArray(VertexArrayID)        
            # disable after drawing command
            glEnableVertexAttribArray(positionID)
           
            # 1st attribute buffer:  vertices
            glEnableVertexAttribArray(0)
            glBindBuffer(GL_ARRAY_BUFFER, grvertexbuffer)
            glVertexAttribPointer(
                0,             # attrib 0, no reason
                4,             # size
                GL_FLOAT,      # type
                GL_FALSE,      # normalized?
                0,             # stride
                c_void_p(0)    # array buffer offset
                )
                
            # 2nd attribute buffer: uvs
            glEnableVertexAttribArray(uvID)
            glBindBuffer(GL_ARRAY_BUFFER, grcolorbuffer)
            glVertexAttribPointer(
                uvID,
                2,
                GL_FLOAT,
                GL_FALSE,
                0,
                c_void_p(0))
            # draw the triangle
            glDrawArrays(GL_TRIANGLES, 0, 2*4) # 3 indices starting at 0

                    
        b,l,f = -1,-1,-1
        t,r,n = 1,1,1
        bln = [b,l,n,1]
        blf = [b,l,f,1]
        brn = [b,r,n,1]
        brf = [b,r,f,1]
        tln = [t,l,n,1]
        tlf = [t,l,f,1]
        trn = [t,r,n,1]
        trf = [t,r,f,1]
        sbot = bln+blf+brn + brn+blf+brf
        stop = tln+tlf+trn + trn+tlf+trf
        sleft = bln+blf+tlf + bln+tlf+tln
        sright = brn+trf+brf + brn+trn+trf
        sfront = bln+tln+trn + bln+trn+brn
        sback = blf+trf+tlf + blf+brf+trf
        
        uvTop = [0,0]+[1,0]+[1,1]+[1,1]+[1,0]+[0,1]             
        uvLeft = [0,0.0]+[0,0.2]+[0.2,0.2]+[0,0.0]+[0.2,0.2]+[0.2,0]
        uvRight = [0.0,1]+[0.2,0.2]+[0.2,0]+[0,1]+[0.2,1]+[0.2,0.4]
        uvFront = [0,0]+[1,0.2]+[1,0.4]+[0,0]+[1,0.4]+[0,1]
        uvBack = [0,0.2]+[0.2,0.4]+[0.2,0.2]+[0,0.2]+[0.4,0]+[0.2,0.4]
        
        
        skybox = sbot
        uvSkybox =  uvTop
        gx_vertex_buffer_data = vec(skybox)
        gx_uv_buffer_data = vec(uvSkybox)
         
        gxvertexbuffer = glGenBuffers(1)        
        glBindBuffer(GL_ARRAY_BUFFER, gxvertexbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx_vertex_buffer_data,GL_STATIC_DRAW)
        
        gxcolorbuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, gxcolorbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx_uv_buffer_data, GL_STATIC_DRAW)
        
        Projection = control.getProjectionMatrix()
        View = control.getViewMatrix()
        Model = N.dot(translationMatrix(0,16,0),scaleMatrix(25,25,25))
        
        MVP = N.dot(Projection, N.dot(View, Model))
        MVP = N.array(MVP, dtype=N.float32)
        #draw into opengl context
        
        
            
        # use our shader
        glUseProgram(programID)
        
        # bind our texture to texture unit
        textureUnit = 4
        glActiveTexture(GL_TEXTURE0 + textureUnit)
        # use our texture data
        glBindTexture(GL_TEXTURE_2D, Texture3)
        # set our sampler to use texture unit
        glUniform1i(TextureID3, textureUnit)      
    
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
    
        glBindVertexArray(VertexArrayID)   
        
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
        glBindVertexArray(VertexArrayID)        
        # disable after drawing command
        glEnableVertexAttribArray(positionID)
       
        # 1st attribute buffer:  vertices
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, gxvertexbuffer)
        glVertexAttribPointer(
            0,             # attrib 0, no reason
            4,             # size
            GL_FLOAT,      # type
            GL_FALSE,      # normalized?
            0,             # stride
            c_void_p(0)    # array buffer offset
            )
            
        # 2nd attribute buffer: uvs
        glEnableVertexAttribArray(uvID)
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
        glVertexAttribPointer(
            uvID,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            c_void_p(0))
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, 2*4) # 3 indices starting at 0
        
        
        Texture4 = loadTexture(os.path.join("images","bhills.png"))
        # get a handle
        TextureID4 = glGetUniformLocation(programID, "myTextureSampler")
        
        skybox2 = stop
        uvSkybox2 =  uvTop
        gx2_vertex_buffer_data = vec(skybox2)
        gx2_uv_buffer_data = vec(uvSkybox2)
         
        gx2vertexbuffer = glGenBuffers(1)        
        glBindBuffer(GL_ARRAY_BUFFER, gx2vertexbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx2_vertex_buffer_data,GL_STATIC_DRAW)
        
        gx2colorbuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, gx2colorbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx2_uv_buffer_data, GL_STATIC_DRAW)
        
        Projection = control.getProjectionMatrix()
        View = control.getViewMatrix()
        Model = N.dot(translationMatrix(0,16,0),scaleMatrix(25,25,25))
        
        MVP = N.dot(Projection, N.dot(View, Model))
        MVP = N.array(MVP, dtype=N.float32)
        #draw into opengl context
        
        
            
        # use our shader
        glUseProgram(programID)
        
        # bind our texture to texture unit
        textureUnit = 4
        glActiveTexture(GL_TEXTURE0 + textureUnit)
        # use our texture data
        glBindTexture(GL_TEXTURE_2D, Texture4)
        # set our sampler to use texture unit
        glUniform1i(TextureID4, textureUnit)      
    
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
    
        glBindVertexArray(VertexArrayID)   
        
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
        glBindVertexArray(VertexArrayID)        
        # disable after drawing command
        glEnableVertexAttribArray(positionID)
       
        # 1st attribute buffer:  vertices
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, gx2vertexbuffer)
        glVertexAttribPointer(
            0,             # attrib 0, no reason
            4,             # size
            GL_FLOAT,      # type
            GL_FALSE,      # normalized?
            0,             # stride
            c_void_p(0)    # array buffer offset
            )
            
        # 2nd attribute buffer: uvs
        glEnableVertexAttribArray(uvID)
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
        glVertexAttribPointer(
            uvID,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            c_void_p(0))
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, 2*4) # 3 indices starting at 0
            

        Texture5 = loadTexture(os.path.join("images","sky.png"))
        # get a handle
        TextureID5 = glGetUniformLocation(programID, "myTextureSampler")
        
        skybox3 = sright
        uvSkybox3 = uvTop
        gx3_vertex_buffer_data = vec(skybox3)
        gx3_uv_buffer_data = vec(uvSkybox3)
         
        gx3vertexbuffer = glGenBuffers(1)        
        glBindBuffer(GL_ARRAY_BUFFER, gx3vertexbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx3_vertex_buffer_data,GL_STATIC_DRAW)
        
        gx3colorbuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, gx3colorbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx3_uv_buffer_data, GL_STATIC_DRAW)
        
        Projection = control.getProjectionMatrix()
        View = control.getViewMatrix()
        Model = N.dot(translationMatrix(0,16,0),scaleMatrix(25,25,25))
        
        MVP = N.dot(Projection, N.dot(View, Model))
        MVP = N.array(MVP, dtype=N.float32)
        #draw into opengl context
        
        
            
        # use our shader
        glUseProgram(programID)
        
        # bind our texture to texture unit
        textureUnit = 4
        glActiveTexture(GL_TEXTURE0 + textureUnit)
        # use our texture data
        glBindTexture(GL_TEXTURE_2D, Texture5)
        # set our sampler to use texture unit
        glUniform1i(TextureID5, textureUnit)      
    
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
    
        glBindVertexArray(VertexArrayID)   
        
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
        glBindVertexArray(VertexArrayID)        
        # disable after drawing command
        glEnableVertexAttribArray(positionID)
       
        # 1st attribute buffer:  vertices
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, gx3vertexbuffer)
        glVertexAttribPointer(
            0,             # attrib 0, no reason
            4,             # size
            GL_FLOAT,      # type
            GL_FALSE,      # normalized?
            0,             # stride
            c_void_p(0)    # array buffer offset
            )
            
        # 2nd attribute buffer: uvs
        glEnableVertexAttribArray(uvID)
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer)
        glVertexAttribPointer(
            uvID,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            c_void_p(0))
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, 2*4) # 3 indices starting at 0

        Texture6 = loadTexture(os.path.join("images","rhills.png"))
        # get a handle
        TextureID6 = glGetUniformLocation(programID, "myTextureSampler")
        
        skybox4 = sback
        uvSkybox4 = [0,0]+[1,1]+[1,0]+[0,0]+[0,1]+[1,1]
        gx4_vertex_buffer_data = vec(skybox4)
        gx4_uv_buffer_data = vec(uvSkybox4)
         
        gx4vertexbuffer = glGenBuffers(1)        
        glBindBuffer(GL_ARRAY_BUFFER, gx4vertexbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx4_vertex_buffer_data,GL_STATIC_DRAW)
        
        gx4colorbuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, gx4colorbuffer)
        glBufferData(GL_ARRAY_BUFFER, gx4_uv_buffer_data, GL_STATIC_DRAW)
        
        Projection = control.getProjectionMatrix()
        View = control.getViewMatrix()
        Model = N.dot(translationMatrix(0,16,0),scaleMatrix(25,25,25))
        
        MVP = N.dot(Projection, N.dot(View, Model))
        MVP = N.array(MVP, dtype=N.float32)
        #draw into opengl context
        
        
            
        # use our shader
        glUseProgram(programID)
        
        # bind our texture to texture unit
        textureUnit = 4
        glActiveTexture(GL_TEXTURE0 + textureUnit)
        # use our texture data
        glBindTexture(GL_TEXTURE_2D, Texture6)
        # set our sampler to use texture unit
        glUniform1i(TextureID6, textureUnit)      
    
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
    
        glBindVertexArray(VertexArrayID)   
        
        glUniformMatrix4fv(MatrixID, 1, GL_TRUE, MVP)
        
        glBindVertexArray(VertexArrayID)        
        # disable after drawing command
        glEnableVertexAttribArray(positionID)
       
        # 1st attribute buffer:  vertices
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, gx4vertexbuffer)
        glVertexAttribPointer(
            0,             # attrib 0, no reason
            4,             # size
            GL_FLOAT,      # type
            GL_FALSE,      # normalized?
            0,             # stride
            c_void_p(0)    # array buffer offset
            )
            
        # 2nd attribute buffer: uvs
        glEnableVertexAttribArray(uvID)
        glBindBuffer(GL_ARRAY_BUFFER, gx4colorbuffer)
        glVertexAttribPointer(
            uvID,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            c_void_p(0))
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, 2*4) # 3 indices starting at 0                        
        # swap buffers
        pygame.display.flip()
        
    # vbo's and vao's are autodeleted by pyopengl
    glDeleteProgram(programID)

        
if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()
        